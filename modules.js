
//#_______________________Constructor__________________________#
const modules = {
    routerPcs: '0x10ED43C718714eb63d5aA57B78B54704E256024E',
    //routerPcs: '0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3', 
    factoryPcs: '0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73', 
    //factoryPcs: '0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73',
    routerEther: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D',
    factoryEther: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
    routerMatic: '0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff',
    factoryMatic: '0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32',
    dxSaleRouter: "0xbaCEbAd5993a19c7188Db1cC8D0F748C9Af1689A",
    buyStatus: false,
    frontRunDetected: false

    //#Testnet wbnb: 0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd , router: 0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3, test: 0xb1ee842fca2390789358B399f164DF8B7E22e88e 
    //#Swap: 0x3d8826c55c554a85eedc4186e0d56ba783c96890 
}
module.exports = modules
