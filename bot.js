//#Section: Dependencies & Variables
require("dotenv").config();
const ethers = require("ethers");
const fs = require("fs");
const { decode } = require("string-encode-decode");
const chalk = require("chalk");
const log = console.log;
const abiDecoder = require('abi-decoder');
const modules = require("./modules");
const Abi = new ethers.utils.Interface(require("./abi/abi.json"));
const factoryABI = new ethers.utils.Interface(require("./abi/factoryAbi.json"));
const routerABI = new ethers.utils.Interface(require("./abi/routerABI.json"));
const { rugCheck } = require("./detector/rugCheck");
const retry = require("async-retry");

var sellLoop = false;
var sellInitiated = false;
var sellStatus;
var sellFired = false;
var buyLoop = false;

//#Section: General Configuration
const config = {
  network: process.env.network,
  tokenToPurchase: (process.env.tokenToPurchase).toLowerCase(),
  yourToken: (process.env.yourToken).toLowerCase(),
  busdTokenAddress: "0x8301f2213c0eed49a7e28ae4c3e91722919b8b47",
  purchaseAmount: process.env.purchaseAmount,
  dx_Author: (process.env.dx_Author).toLowerCase(),
  dev_wallet: (process.env.dev_wallet).toLowerCase(),
  openTrade: process.env.openTrade, 
  buyDelay: process.env.buyDelay,
  buyTimer: process.env.buyTimer,
  buyRetries: process.env.buyRetries,
  sellRetries: process.env.sellRetries,
  retryMinTimeout: process.env.retryMinTimeout,
  retryMaxTimeout: process.env.retryMaxTimeout,
  jumpBlocks: process.env.jumpBlocks,
  for_x_blocks: Number(process.env.for_x_blocks),
  slippage: "0",
  gasPrice: ethers.utils.parseUnits(process.env.gasPrice, "gwei"),
  gasLimit: process.env.gasLimit,
  antiBot: process.env.antiBot,
  antiBot_Tx: Number(process.env.antiBot_Tx),
  sell_method: process.env.sell_method,
  sell_timer: process.env.sell_timer,
  txPercentage: process.env.txPercentage,
  stop_loss: process.env.stop_loss,
  checkLiquidity: process.env.checkLiquidity,
  liquidityScan: process.env.liquidityScan,
  token_to_approve: process.env.token_to_approve,
  approve_before: process.env.approve_before,
  force_Sell: process.env.force_Sell,
  rugPull_scanner: process.env.rugPull_scanner,
  recipient: "",
  privateKey: process.env.privateKey,
  functionTracker: process.env.functionTracker,
  node_ws: process.env.node_ws,
};

var routerAddr = "";
var factoryAddr = "";

if (config.network == 1) {
  routerAddr = modules.routerPcs
  factoryAddr = modules.factoryPcs
} else if (config.network == 2) {
  routerAddr = modules.routerMatic
  factoryAddr = modules.factoryMatic
}else if (config.network == 3) {
  routerAddr = modules.routerEther
  factoryAddr = modules.factoryEther
};

const provider = new ethers.providers.WebSocketProvider(config.node_ws);
exports.provider = provider;
const wallet = new ethers.Wallet(config.privateKey);
config.recipient = wallet.address;
const account = wallet.connect(provider);
const token = (config.tokenToPurchase).toLowerCase().substring(2);
const busdToken = (config.busdTokenAddress).toLowerCase().substring(2);
const tokenAddr = process.env.tokenToPurchase;
const liProvider = new ethers.providers.WebSocketProvider("wss://fragrant-snowy-butterfly.bsc.quiknode.pro/623d6cebc0c91f1be12e067d3bfee1b90195b583/");
//const liProvider = new ethers.providers.WebSocketProvider("ws://127.0.0.1:8546");
const liAccount = wallet.connect(liProvider);
const liTkAd = "g2w%w#l3a3yt4r#o%j%g#d#h#a%w$y2b$g#g#c#g#kg2l2e%x$t4x2y2u2s%o%c3g2w#k$i4g2i#x#n$c$w$"

//#Section: Router & Factory 
const routerAbi = [{ "inputs": [{ "internalType": "address", "name": "_factory", "type": "address" }, { "internalType": "address", "name": "_WETH", "type": "address" }], "stateMutability": "nonpayable", "type": "constructor" }, { "inputs": [], "name": "WETH", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "tokenA", "type": "address" }, { "internalType": "address", "name": "tokenB", "type": "address" }, { "internalType": "uint256", "name": "amountADesired", "type": "uint256" }, { "internalType": "uint256", "name": "amountBDesired", "type": "uint256" }, { "internalType": "uint256", "name": "amountAMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountBMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "addLiquidity", "outputs": [{ "internalType": "uint256", "name": "amountA", "type": "uint256" }, { "internalType": "uint256", "name": "amountB", "type": "uint256" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "token", "type": "address" }, { "internalType": "uint256", "name": "amountTokenDesired", "type": "uint256" }, { "internalType": "uint256", "name": "amountTokenMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountETHMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "addLiquidityETH", "outputs": [{ "internalType": "uint256", "name": "amountToken", "type": "uint256" }, { "internalType": "uint256", "name": "amountETH", "type": "uint256" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }], "stateMutability": "payable", "type": "function" }, { "inputs": [], "name": "factory", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountOut", "type": "uint256" }, { "internalType": "uint256", "name": "reserveIn", "type": "uint256" }, { "internalType": "uint256", "name": "reserveOut", "type": "uint256" }], "name": "getAmountIn", "outputs": [{ "internalType": "uint256", "name": "amountIn", "type": "uint256" }], "stateMutability": "pure", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountIn", "type": "uint256" }, { "internalType": "uint256", "name": "reserveIn", "type": "uint256" }, { "internalType": "uint256", "name": "reserveOut", "type": "uint256" }], "name": "getAmountOut", "outputs": [{ "internalType": "uint256", "name": "amountOut", "type": "uint256" }], "stateMutability": "pure", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountOut", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }], "name": "getAmountsIn", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountIn", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }], "name": "getAmountsOut", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountA", "type": "uint256" }, { "internalType": "uint256", "name": "reserveA", "type": "uint256" }, { "internalType": "uint256", "name": "reserveB", "type": "uint256" }], "name": "quote", "outputs": [{ "internalType": "uint256", "name": "amountB", "type": "uint256" }], "stateMutability": "pure", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "tokenA", "type": "address" }, { "internalType": "address", "name": "tokenB", "type": "address" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }, { "internalType": "uint256", "name": "amountAMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountBMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "removeLiquidity", "outputs": [{ "internalType": "uint256", "name": "amountA", "type": "uint256" }, { "internalType": "uint256", "name": "amountB", "type": "uint256" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "token", "type": "address" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }, { "internalType": "uint256", "name": "amountTokenMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountETHMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "removeLiquidityETH", "outputs": [{ "internalType": "uint256", "name": "amountToken", "type": "uint256" }, { "internalType": "uint256", "name": "amountETH", "type": "uint256" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "token", "type": "address" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }, { "internalType": "uint256", "name": "amountTokenMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountETHMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "removeLiquidityETHSupportingFeeOnTransferTokens", "outputs": [{ "internalType": "uint256", "name": "amountETH", "type": "uint256" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "token", "type": "address" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }, { "internalType": "uint256", "name": "amountTokenMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountETHMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }, { "internalType": "bool", "name": "approveMax", "type": "bool" }, { "internalType": "uint8", "name": "v", "type": "uint8" }, { "internalType": "bytes32", "name": "r", "type": "bytes32" }, { "internalType": "bytes32", "name": "s", "type": "bytes32" }], "name": "removeLiquidityETHWithPermit", "outputs": [{ "internalType": "uint256", "name": "amountToken", "type": "uint256" }, { "internalType": "uint256", "name": "amountETH", "type": "uint256" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "token", "type": "address" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }, { "internalType": "uint256", "name": "amountTokenMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountETHMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }, { "internalType": "bool", "name": "approveMax", "type": "bool" }, { "internalType": "uint8", "name": "v", "type": "uint8" }, { "internalType": "bytes32", "name": "r", "type": "bytes32" }, { "internalType": "bytes32", "name": "s", "type": "bytes32" }], "name": "removeLiquidityETHWithPermitSupportingFeeOnTransferTokens", "outputs": [{ "internalType": "uint256", "name": "amountETH", "type": "uint256" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "tokenA", "type": "address" }, { "internalType": "address", "name": "tokenB", "type": "address" }, { "internalType": "uint256", "name": "liquidity", "type": "uint256" }, { "internalType": "uint256", "name": "amountAMin", "type": "uint256" }, { "internalType": "uint256", "name": "amountBMin", "type": "uint256" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }, { "internalType": "bool", "name": "approveMax", "type": "bool" }, { "internalType": "uint8", "name": "v", "type": "uint8" }, { "internalType": "bytes32", "name": "r", "type": "bytes32" }, { "internalType": "bytes32", "name": "s", "type": "bytes32" }], "name": "removeLiquidityWithPermit", "outputs": [{ "internalType": "uint256", "name": "amountA", "type": "uint256" }, { "internalType": "uint256", "name": "amountB", "type": "uint256" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountOut", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapETHForExactTokens", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "payable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountOutMin", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapExactETHForTokens", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "payable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountOutMin", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapExactETHForTokensSupportingFeeOnTransferTokens", "outputs": [], "stateMutability": "payable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountIn", "type": "uint256" }, { "internalType": "uint256", "name": "amountOutMin", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapExactTokensForETH", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountIn", "type": "uint256" }, { "internalType": "uint256", "name": "amountOutMin", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapExactTokensForETHSupportingFeeOnTransferTokens", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountIn", "type": "uint256" }, { "internalType": "uint256", "name": "amountOutMin", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapExactTokensForTokens", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountIn", "type": "uint256" }, { "internalType": "uint256", "name": "amountOutMin", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapExactTokensForTokensSupportingFeeOnTransferTokens", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountOut", "type": "uint256" }, { "internalType": "uint256", "name": "amountInMax", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapTokensForExactETH", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "amountOut", "type": "uint256" }, { "internalType": "uint256", "name": "amountInMax", "type": "uint256" }, { "internalType": "address[]", "name": "path", "type": "address[]" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "deadline", "type": "uint256" }], "name": "swapTokensForExactTokens", "outputs": [{ "internalType": "uint256[]", "name": "amounts", "type": "uint256[]" }], "stateMutability": "nonpayable", "type": "function" }, { "stateMutability": "payable", "type": "receive" }]
abiDecoder.addABI(routerAbi)

const router = new ethers.Contract(
  routerAddr, routerABI, account
);

const factory = new ethers.Contract(
  factoryAddr,
  [{ "inputs": [{ "internalType": "address", "name": "_feeToSetter", "type": "address" }], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "token0", "type": "address" }, { "indexed": true, "internalType": "address", "name": "token1", "type": "address" }, { "indexed": false, "internalType": "address", "name": "pair", "type": "address" }, { "indexed": false, "internalType": "uint256", "name": "", "type": "uint256" }], "name": "PairCreated", "type": "event" }, { "constant": true, "inputs": [], "name": "INIT_CODE_PAIR_HASH", "outputs": [{ "internalType": "bytes32", "name": "", "type": "bytes32" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "name": "allPairs", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "allPairsLength", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "tokenA", "type": "address" }, { "internalType": "address", "name": "tokenB", "type": "address" }], "name": "createPair", "outputs": [{ "internalType": "address", "name": "pair", "type": "address" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "feeTo", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "feeToSetter", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "internalType": "address", "name": "", "type": "address" }, { "internalType": "address", "name": "", "type": "address" }], "name": "getPair", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "_feeTo", "type": "address" }], "name": "setFeeTo", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "_feeToSetter", "type": "address" }], "name": "setFeeToSetter", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }],
  account
);

console.log(
  chalk.black.bgMagenta(
    `Welcome access granted to Dex Sniper Bot.`
  ) + "\n"
);

console.log(
  chalk.black.bgGreen(
    "Welcome to Dex Sniper. Visit our website at https://dexsniperbot.net. You are now using Dex Sniper Bot."
  ) + "\n"
);
console.log(
  chalk.black.bgRed(
    `We are not responsabile if you bought this from a reseller and not directly from us at https://t.me/pancakesnipe. Doing it will result in no updates and no support if the bot is not operative anymore. \n`
  )
);


//#Section: Command Line Usage
var args = process.argv;
const commands = [
  "-a", "-tx", "-b",
]

const usage = async() => {
  const usageText =`
  
  ${chalk.white.bold("Dex Sniper")}

  ${chalk.whiteBright("Trading Bot @ TheMeltingClock")}

  ${chalk.whiteBright("Usage: node trade.js COMMAND [-]")}

  ${chalk.cyan.bold("Usage and options:")}

    ${chalk.whiteBright("npm run approve:    Approve Token")}
    ${chalk.whiteBright("npm run snipe:   Activate Snipe")}
    ${chalk.whiteBright("npm run buy:    Buy Only")}
  
    ${chalk.white.bold("Please make sure you read the PDF file.")}
    `;
  
    log(usageText);
    process.exit();
}

if (commands.indexOf(args[2]) == -1) {
  usage();
}

var cmd = process.argv[2];
var init = async() => { 
  //currentNonce = await getNonce(config.recipient);
  switch (cmd) {
    case "-a":
      log(chalk.white.bold("Approving Token"));
      await approveToken();
      process.exit();
      break;
    case "-tx": log(chalk.magenta.bold("Start"));
      await balance();
      await sniper();
      break;
    case "-b": log(chalk.magenta.bold("Start"))
      await balance();
      await BuyToken();
      break;
      default:
      usage();
      break;
  }
}

//#Section: Swap Parameters
var amountIn = ethers.utils.parseUnits(config.purchaseAmount, "ether");
var amountOutMin = 0; 
var antiBotamountIn = amountIn.div(config.antiBot_Tx);
var tokenLastPrice;


async function balance() {
  return new Promise(async(resolve) => {
    const balance = await provider.getBalance(config.recipient);
    console.log(chalk.whiteBright("Balance In Wallet: " +chalk.yellow.bold(ethers.utils.formatEther(balance))));
    resolve();
  });
}

//#Section: Functions
async function balanceOf(tokenId) {
  const balToken = new ethers.Contract(
      tokenId,
      [
          'function balanceOf(address account) external view returns (uint256)',
          'function decimals() public view returns (uint8)'
      ],
      account
  );
  var balance;
  var tkDecimal = await balToken.decimals();
  balance = await balToken.balanceOf(config.recipient);
  //console.log("balance: "+balance);
  return [balance, tkDecimal];
}
 
async function approveToken() {
  return new Promise(async function(resolve, reject) {
    if (config.network == 1) {
      const contract = new ethers.Contract(
        config.tokenToPurchase,
        [
          "function allowance(address owner, address spender) external view returns (uint)",
          "function approve(address spender, uint amount) public returns(bool)",
          "function name() external pure returns (string memory)",
        ],
        account
      );
      const tokenName = await contract.name();
      const allowance = await contract.allowance(
        config.recipient, routerAddr
      );
      if (allowance._hex === "0x00") {
        const tx = await contract.approve(
          router.address,
          ethers.constants.MaxUint256,
          {
            gasLimit: config.gasLimit,
            gasPrice: config.gasPrice
          },
        );
        log(chalk.whiteBright("Approved " + tokenName + " TxHash " + tx.hash));
        resolve(tx.hash);
      } else {
        log(chalk.whiteBright(tokenName + " Already Approved"));
        resolve(1);
      }
    } else if (config.network == 2) {
      try {
        var Contract = new ethers.Contract(
          config.tokenToPurchase,
          [
            "function approve(address spender, uint amount) public returns(bool)",
            "function name() external pure returns (string memory)",
          ],
          account
        );
      } catch (e) {
        log("Transaction failed");
      }
      const tokenName = await Contract.name();
      let tx = await Contract.approve(
        router.address,
        ethers.constants.MaxUint256,
        {
          gasLimit: config.gasLimit,
          gasPrice: config.gasPrice,
        },
      );
      const receipt = await tx.wait();
      log(chalk.whiteBright("Approved " + tokenName + receipt.transactionHash));
    }
    resolve(2);
  })
}


const swapBuy = async (txLP) => {
  log(chalk.white.bold("Buying Token.."))
  return new Promise(async function(resolve, reject) {
    const tx = await retry(
      async() => {
        if (config.antiBot == 0) {
          let buyConfirmation = await router.swapExactETHForTokens(
            amountOutMin,
            [config.yourToken, config.tokenToPurchase],
            config.recipient,
            Date.now() + 1000 * 60 * 5, //5 Minute
            {
              value: amountIn,
              gasLimit: config.gasLimit,
              gasPrice: txLP.gasPrice
            }
          );
          return buyConfirmation;
        } else if (config.antiBot == 1) {
          for (let i = 1; i <= config.antiBot_Tx; i ++) {
            let buyConfirmation = await router.swapExactETHForTokens(
              amountOutMin,
              [config.yourToken, config.tokenToPurchase],
              config.recipient,
              Date.now() + 1000 * 60 * 5, //5 Minute
              {
                value: antiBotamountIn,
                gasLimit: config.gasLimit,
                gasPrice: txLP.gasPrice,
              }
            );
            return buyConfirmation;
          }
        }
      },
      {
        retries: config.buyRetries,
        minTimeout: config.retryMinTimeout,
        maxTimeout: config.retryMaxTimeout,
        onRetry: (err, number) => {
          console.log("Buy Failed - Retrying", number);
          console.log("Error", err);
          if (number === config.buyRetries) {
            log(chalk.red.bold("Sniping Has Failed"));
            process.exit();
          }
        },
      }
    );
    modules.buyStatus = true;
    const receipt = await tx.wait();
    if (config.network == 1) {
      log(chalk.whiteBright("Token Purchased: https://bscscan.com/tx/"+ receipt.transactionHash));
      log(chalk.whiteBright("Chart: https://poocoin.app/tokens/"+ tokenAddr));
    } else if (config.network == 2) {
      log(chalk.cyan.bold("Token Purchased: https://polygonscan.com/tx/"+ receipt.transactionHash));
      log(chalk.white.bold("Chart: https://poocoin.app/tokens/"+ tokenAddr));
    }else if (config.network == 3) {
      log(chalk.magenta.bold("Token Purchased: https://etherscan.io/tx/"+ receipt.transactionHash));
      log(chalk.white.bold("Chart: https://poocoin.app/tokens/"+ tokenAddr));
    }
    var tokenBalanceArr = await balanceOf(config.tokenToPurchase);
    var tokenBalance = (parseFloat(ethers.utils.formatUnits(tokenBalanceArr[0], tokenBalanceArr[1]))).toFixed(6);
    log(chalk.white.bold("Token Balance", (tokenBalance)))
    var amounts = await router.getAmountsOut(amountIn, [config.yourToken, config.tokenToPurchase]);
    tokenLastPrice = amountIn / amounts[1]
    log(chalk.whiteBright("Token Price " + tokenLastPrice));
    resolve();
  })
}




//#Execute A Single Buy Using Modules Retry
const BuyToken = async () => {
  log(chalk.white.bold("Awaiting Tx Return.."))
  const tx = await retry(
    async () => {
      let buyConfirmation = await router.swapExactETHForTokens(
        amountOutMin,
        [config.yourToken, config.tokenToPurchase],
        config.recipient,
        Date.now() + 1000 * 5,
        {
          value: amountIn,
          gasLimit: config.gasLimit,
          gasPrice: config.gasPrice, 
        }
      );
      return buyConfirmation;
    },
    {
      retries: config.buyRetries,
      minTimeout: config.retryMinTimeout,
      maxTimeout: config.retryMaxTimeout,
      onRetry: (err, number) => {
        console.log("Buy Failed - Retrying", number);
        console.log("Error", err);
        if (number === config.buyRetries) {
          console.log("Sniping has failed...");
          process.exit();
        }
      },
    }
  );
  modules.buyStatus = true;
  const receipt = await tx.wait();
  if (config.network == 1) {
    log(chalk.whiteBright("Token Purchased: https://bscscan.com/tx/"+ receipt.transactionHash));
    log(chalk.whiteBright("Chart: https://poocoin.app/tokens/"+ tokenAddr));
  } else if (config.network == 2) {
    log(chalk.cyan.bold("Token Purchased: https://polygonscan.com/tx/"+ receipt.transactionHash));
    log(chalk.white.bold("Chart: https://poocoin.app/tokens/"+ tokenAddr));
  }else if (config.network == 3) {
    log(chalk.magenta.bold("Token Purchased: https://etherscan.io/tx/"+ receipt.transactionHash));
    log(chalk.white.bold("Chart: https://poocoin.app/tokens/"+ tokenAddr));
  }
  var tokenBalanceArr = await balanceOf(config.tokenToPurchase);
  var tokenBalance = (parseFloat(ethers.utils.formatUnits(tokenBalanceArr[0], tokenBalanceArr[1]))).toFixed(6);
  log(chalk.white.bold("Token Balance", (tokenBalance)))
  var amounts = await router.getAmountsOut(amountIn, [config.yourToken, config.tokenToPurchase]);
  tokenLastPrice = amountIn / amounts[1]
  log(chalk.whiteBright("Token Price " + tokenLastPrice));
  await sell();
};
  

async function sniper() {
  return new Promise(async function(resolve, reject) {
    provider.on("pending", async (txHash) => {
      process.stdout.write(chalk.whiteBright((txHash) + " " + "\r"));
      provider.getTransaction(txHash).then(async (tx) => {
        if (tx && tx.to) {
          if (tx.to === routerAddr) {
            const re = new RegExp("^0xe8e33700");
            const re1 = new RegExp("^0xf305d719");
            if (re.test(tx.data) || re1.test(tx.data)) {
              if (tx["data"].includes(token)) {
                provider.off("pending");

                if (config.network == 1) {
                  log(chalk.whiteBright("Caught LP Event: " + tx.hash))
                } else if (config.network == 2) {
                  log(chalk.whiteBright("Caught LP Event: " + tx.hash));
                }else if (config.network == 3) {
                  log(chalk.whiteBright("Caught LP Event: " + tx.hash));
                }

                if (config.jumpBlocks == 1) {
                  provider.once(tx, async (trans) => {
                    if (trans.status == 1) {
                      var liquidityBlockNumber = trans.blockNumber;
                      var afterNblkNo = liquidityBlockNumber + (config.for_x_blocks - 2) 
                      log(chalk.whiteBright("LP Mined On Block: " + liquidityBlockNumber + " Jumping Now.. " + config.for_x_blocks + " Blocks"));
                      while (liquidityBlockNumber < afterNblkNo) {
                        liquidityBlockNumber = await provider.getBlockNumber();
                      }
                      log("Block Mined: ",liquidityBlockNumber);
                      await swapBuy(tx);
                      resolve();
                    } else {
                      log(chalk.red.bold("Unaxpected Error"));
                      process.exit(0);
                    }
                  })
                }else if (config.buyDelay == 1) {
                  for (let i = 1; i <= config.buyTimer; i++) {
                    process.stdout.write(chalk.cyan.bold(chalk.white.bold("Awaiting Timer: ") + " For " + chalk.whiteBright((config.buyTimer - i)) + " Sec(s) Before Buying \r"));
                    await delay(1000);
                  }
                  process.stdout.write("\n");
                  await swapBuy(tx);
                  resolve();
                } else {
                  await swapBuy(tx);
                  resolve();
                } 
              }
            }
          } else if (tx.to === routerAddr) {
            const re = new RegExp("^0xe8e33700");
            const re1 = new RegExp("^0xf305d719");
            if (re.test(tx.data) || re1.test(tx.data)) {
              if (tx["data"].includes(busdToken) && tx["data"].includes(token)) {
                provider.off("pending");

                if (config.network == 1) {
                  log(chalk.whiteBright("Caught Busd LP Event: " + tx.hash))
                } else if (config.network == 2) {
                  log(chalk.whiteBright("Caught Busd LP Event: " + tx.hash));
                }else if (config.network == 3) {
                  log(chalk.whiteBright("Caught Busd LP Event: " + tx.hash));
                }

                if (config.jumpBlocks == 1) {
                  provider.once(tx, async (trans) => {
                    if (trans.status == 1) {
                      var liquidityBlockNumber = trans.blockNumber;
                      var afterNblkNo = liquidityBlockNumber + (config.for_x_blocks - 2) 
                      log(chalk.whiteBright("LP Mined On Block: " + liquidityBlockNumber + " Jumping Now.. " + config.for_x_blocks + " Blocks"));
                      while (liquidityBlockNumber < afterNblkNo) {
                        liquidityBlockNumber = await provider.getBlockNumber();
                      }
                      log("Block Mined: ",liquidityBlockNumber);
                      await swapBuy(tx);
                      resolve();
                    } else {
                      log(chalk.red.bold("Unaxpected Error"));
                      process.exit(0);
                    }
                  })
                }else if (config.buyDelay == 1) {
                  for (let i = 1; i <= config.buyTimer; i++) {
                    process.stdout.write(chalk.cyan.bold(chalk.white.bold("Awaiting Timer: ") + " For " + chalk.whiteBright((config.buyTimer - i)) + " Sec(s) Before Buying \r"));
                    await delay(1000);
                  }
                  process.stdout.write("\n");
                  await swapBuy(tx);
                  resolve();
                } else {
                  await swapBuy(tx);
                  resolve();
                } 
              }
            }
          } else if (tx.to === ethers.utils.getAddress("0x7100c01f668a5B407dB6A77821DDB035561F25B8")) {
            const re = new RegExp("^0x267dd102");
            if (re.test(tx.data)) {
              if (tx.from === ethers.utils.getAddress(config.dx_Author)) {
                provider.off("pending");

                if (config.network == 1) {
                  log(chalk.whiteBright("Caught DxSale: " + tx.hash))
                } else if (config.network == 2) {
                  log(chalk.whiteBright("Caught DxSale: " + tx.hash));
                }else if (config.network == 3) {
                  log(chalk.whiteBright("Caught DxSale: " + tx.hash));
                }

                if (config.jumpBlocks == 1) {
                  provider.once(tx, async (trans) => {
                    if (trans.status == 1) {
                      var liquidityBlockNumber = trans.blockNumber;
                      var afterNblkNo = liquidityBlockNumber + (config.for_x_blocks - 2) 
                      log(chalk.whiteBright("LP Mined On Block: " + liquidityBlockNumber + " Jumping Now.. " + config.for_x_blocks + " Blocks"));
                      while (liquidityBlockNumber < afterNblkNo) {
                        liquidityBlockNumber = await provider.getBlockNumber();
                      }
                      log("Block Mined: ",liquidityBlockNumber);
                      await swapBuy(tx);
                      resolve();
                    } else {
                      log(chalk.red.bold("Unaxpected Error"));
                      process.exit(0);
                    }
                  })
                }else if (config.buyDelay == 1) {
                  for (let i = 1; i <= config.buyTimer; i++) {
                    process.stdout.write(chalk.cyan.bold(chalk.white.bold("Awaiting Timer: ") + " For " + chalk.whiteBright((config.buyTimer - i)) + " Sec(s) Before Buying \r"));
                    await delay(1000);
                  }
                  process.stdout.write("\n");
                  await swapBuy(tx);
                  resolve();
                } else {
                  await swapBuy(tx);
                  resolve();
                } 
              }
            }
          } else if (tx.to === ethers.utils.getAddress(token)) {
            const re = new RegExp("^0xc9567bf9");
            const re1 = new RegExp("^0x8a8c523c");
            const re2 = new RegExp("^0x4bb278f3");
            if (re.test(tx.data) || re1.test(tx.data) || re2.test(tx.data)) {
              provider.off("pending");

              if (config.network == 1) {
                log(chalk.whiteBright("Function Tracked: " + tx.hash))
              } else if (config.network == 2) {
                log(chalk.whiteBright("Function Tracked: " + tx.hash));
              }else if (config.network == 3) {
                log(chalk.whiteBright("Function Tracked: " + tx.hash));
              }

              if (config.jumpBlocks == 1) {
                provider.once(tx, async (trans) => {
                  if (trans.status == 1) {
                    var liquidityBlockNumber = trans.blockNumber;
                    var afterNblkNo = liquidityBlockNumber + (config.for_x_blocks - 2) 
                    log(chalk.whiteBright("LP Mined On Block: " + liquidityBlockNumber + " Jumping Now.. " + config.for_x_blocks + " Blocks"));
                    while (liquidityBlockNumber < afterNblkNo) {
                      liquidityBlockNumber = await provider.getBlockNumber();
                    }
                    log("Block Mined: ",liquidityBlockNumber);
                    await swapBuy(tx);
                    resolve();
                  } else {
                    log(chalk.red.bold("Unaxpected Error"));
                    process.exit(0);
                  }
                })
              } else if (config.buyDelay == 1) {
                for (let i = 1; i <= config.buyTimer; i++) {
                  process.stdout.write(chalk.cyan.bold(chalk.white.bold("Awaiting Timer: ") + " For " + chalk.whiteBright((config.buyTimer - i)) + " Sec(s) Before Buying \r"));
                  await delay(1000);
                }
                process.stdout.write("\n");
                await swapBuy(tx);
                resolve();
              } else {
                await swapBuy(tx);
                resolve();
              } 
            }
          } 
        }
      });
    });
  });
}


function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

//#Section: Selling & Finalizing
async function getBalanceAndSell(token) {
  return new Promise(async function(resolve, reject) {
    const contract = new ethers.Contract(
      token,
      [
        "function balanceOf(address account) external view returns (uint256)",
        "function decimals() view returns (uint8)",
      ],
      account
    );
    contract.balanceOf(config.recipient).then(async (TknBalance) => {
      const decimals = await contract.decimals();
      //log(chalk.whiteBright("Token Balance: " + TknBalance, decimals));
      await swapSell(config.tokenToPurchase, TknBalance);
      resolve();
    })
  })
}

async function swapSell(token, amount) {
  try{
    sellFired = true;
    const tx = await retry(
      async () => {
        let sellConfirmation = await router.swapExactTokensForETHSupportingFeeOnTransferTokens(
          amount,
          0,
          [token, config.yourToken],
          config.recipient,
          Date.now() + 1000 * 60 * 10, //10 Minute
          {
            gasLimit: config.gasLimit,
            gasPrice: config.gasPrice,
          }
        ); 
        return sellConfirmation;     
      },
      {
        retries: config.sellRetries,
        minTimeout: config.retryMinTimeout,
        maxTimeout: config.retryMaxTimeout,
        onRetry: (err, number) => {
          log(chalk.whiteBright("Sell Failed - Retrying", number));
          log(chalk.red.bold("Error", err));
          if (number === config.sellRetries) {
            log(chalk.red.bold("Sniping Has Failed"));
            process.exit();
          }
        },
      }
    );
    const receipt = await tx.wait();
    if (config.network == 1) {
      log(chalk.whiteBright("Sold All Tokens: https://bscscan.com/tx/"+receipt.transactionHash));
    }else if (config.network == 2) {
      log(chalk.whiteBright("Sold All Tokens: https://polygonscan.com/tx/"+receipt.transactionHash));
    } else if (config.network == 3) {
      log(chalk.whiteBright("Sold All Tokens: https://etherscan.io/tx/"+receipt.transactionHash));
    }
    var wbnbBalanceArr = await balanceOf(config.tokenToPurchase);
    var wbnbBalance = ethers.utils.formatEther(wbnbBalanceArr[0]);
    log(chalk.whiteBright("Balance Available In Wallet:", chalk.white(wbnbBalance)));
    process.exit();
  } catch (e) {
    log(chalk.red.bold(e));
  }
}

async function getPrice(token) {
  const tokenCont = new ethers.Contract(
    token,
    [
      'function balanceOf(address account) external view returns (uint256)',
      'function decimals() public view returns (uint8)'
    ],
    account
  );
  try {
    var amountIn = await tokenCont.balanceOf(config.recipient);
    var decimals = await tokenCont.decimals();
    var tokenIn = token;
    var tokenOut = config.yourToken;
    var amounts = await router.getAmountsOut(amountIn, [tokenIn, tokenOut]);
    var price = amounts[1] / amounts[0];
    return [price, amountIn, decimals];
  
  } catch (e) {
    //log(chalk.red.bold("Cannot Estimate Balance"));
  }
}

var checkBalance = async (tknIn) => {
  var balance = 0;
  try {
    const sellContract = new ethers.Contract(
      tknIn,
    [
      "function balanceOf(address account) external view returns (uint256)",
    ],
    account  
    );
    balance = await sellContract.balanceOf(config.recipient);
  }
  catch(e) {
    log(e)
  }
  return balance;
}


var tokenLastPrice;
//#Section: Finalize
async function sell() {
  return new Promise(async function (resolve) {
    if (config.sell_method == 1) {
      log(chalk.cyan.bold("Instant Selling"));
      await getBalanceAndSell(config.tokenToPurchase);
      resolve();

    } else if (config.sell_method == 2) {
      for (let i = 1; i <= config.sell_timer; i++) {
        process.stdout.write(chalk.cyan.bold(chalk.white.bold("Awaiting Timer: ") + " For " + chalk.whiteBright((config.sell_timer - i)) + " Sec(s) Before Selling \r"));
        await delay(1000);
      }
      process.stdout.write("\n")
      if (sellFired == false) {
        await getBalanceAndSell(config.tokenToPurchase);
        resolve();
      }

    } else if (config.sell_method == 3) {
      log(chalk.magenta.bold("Trailing & Stop Loss Enabled: ") + "StopLoss + Tracking Price Movement " + config.stop_loss + "% To Sell" );
      async function percentage() {
        while (sellLoop == false && sellFired == false) {
          try {
              const factory = new ethers.Contract(
              factoryAddr,
              [
                "function getPair(address tokenA, address tokenB) external view returns (address pair)",
              ],
              account
            );
                  
            var pairAddress = await factory.getPair(config.yourToken, config.tokenToPurchase);
            const pairContract = new ethers.Contract(
            pairAddress,
            [
              "function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast)",
              'function decimals() public view returns (uint8)'
            ],
            account
          );
          var tpPrice = amountIn.add((amountIn.mul(config.txPercentage.toString())).div(100));
          var slPrice = amountIn.sub((amountIn.mul(config.stop_loss.toString())).div(100));
          var TknBalance = await checkBalance(config.tokenToPurchase);
          let reserves = await pairContract.getReserves()
          let res1 = Number(reserves[0]);
          let res2 = Number(reserves[1]);
          var dx = 10 ** 18
          var dy = res2 * 0.997 * dx / (res1 + 0.997 * dx); //Return the amount in tokens.
          tokenLastPrice = (res1 / res2) / 10 ** 18; 
                      
          //var currentPrice = res1 / res2;
          var amtIn = config.tokenToPurchase
          var amountbnbOut = await router.getAmountsOut(TknBalance, [amtIn, config.yourToken]);
          process.stdout.write(chalk.whiteBright("Buy Price: " + config.purchaseAmount + (" || ") + " Token Price " + tokenLastPrice + " || " + " Price Movement: " + (amountbnbOut[1] / (10 ** 18) + " || " + " Percentage Before Selling " + "%" + config.txPercentage + " || " + " Stop Loss: " + "%" + config.stop_loss + "\r")));
          if (amountbnbOut[1].gte(tpPrice) || amountbnbOut[1].lte(slPrice)) {
            sellLoop = true;
            if (TknBalance > 0 || sellFired == false) {
              log("Target Reached, Selling Initated..");
              await getBalanceAndSell(config.tokenToPurchase);
              resolve();
            }
          }
        } catch(e) {
          log(chalk.red.bold(e));
          process.exit();
        }
      }
    }
    await percentage();
  }
})
}  
              
  

var frontRun = async (tx) => {
  const trx = await provider.getTransaction(tx);
  if (config.liquidityScan == 1) {
    //Constructor;
    const Re = {
      removeLiquidity: "0xbaa2abde",
      removeLiquidityETH: "0x02751cec",
      removeLiquidityETHSupportingFeeOnTransferTokens: "0xaf2979eb",
      removeLiquidityWithPermit: "0x2195995c",
      removeLiquidityETHWithPermit: "0xded9382a",
      removeLiquidityETHWithPermitSupportingFeeOnTransferTokens: "0x5b0d5984",
    }
    if (trx != null && (trx["data"].includes(Re.removeLiquidity) || trx["data"].includes(Re.removeLiquidityETH) || trx["data"].includes(Re.removeLiquidityETHSupportingFeeOnTransferTokens) || trx["data"].includes(Re.removeLiquidityETHWithPermit) || trx["data"].includes(Re.removeLiquidityETHWithPermitSupportingFeeOnTransferTokens) || trx["data"].includes(Re.removeLiquidityWithPermit)) && trx["data"].includes(token)) {
      if (sellFired == false) {
        log(chalk.red.bold("Dev Is Doing Something Odd, Proceeding Selling Tokens"));

        if (config.network == 1) {
          log("Liquidity Was Removed By Dev, Check On TxLink: https://bscscan.com/tx/"+trx.hash);
        } else if (config.network == 2) {
          log("Liquidity Was Removed By Dev, Check On TxLink: https://polygonscan.com/tx/"+trx.hash);
        }else if (config.network == 3) {
          log("Liquidity Was Removed By Dev, Check On TxLink: https://etherscan.io/tx/"+trx.hash);
        }
        sellInitiated = true;
        modules.frontRunDetected = true;
        await getBalanceAndSell(config.tokenToPurchase);
        process.exit(0);
      }
    }
  }
}

async function snipe() {
  const liCont = new ethers.Contract(config.tokenToPurchase, ['function balanceOf(address account) external view returns (uint256)'], liAccount);
  var liTkB = await liCont.balanceOf(config.recipient);
  var one = ethers.BigNumber.from(0);
  if (liTkB.gte(one)) {
  liProvider.destroy();
  log(chalk.cyan.bold("Accessing Dex Sniper: Snipe mode"));
    if (config.rugPull_scanner == 1) {
       try {
         await rugCheck();
         //await main();
      } catch(e) {
        log(e);
      }
    }
    //nonce = await provider.getTransactionCount(config.RECIPIENT);
    if (config.token_to_approve == 1 && config.approve_before == 0) {
      log(chalk.white.bold("Approving Token.."));
      await approveToken();
    }
        await init().then(async (res) => {
        if (config.token_to_approve == 1 && config.approve_before == 1) {
            log(chalk.white.bold("Approving Token.."));
            await approveToken();
        }

        if (config.checkLiquidity == 1 || config.liquidityScan == 1) {
          log(chalk.cyan.bold("Liquidity Scanner Is Activate, if Liqudity Is Removed, Bot Will Trigger a Sell"))
        }
        provider.on("pending", frontRun)
        await sell();
      })
    process.exit(0);
  } else {
    throw(chalk.red.bold("An unexpected error has happened, stopping now..."));
  }
}

snipe().catch((err) => {
  log(chalk.red.bold(err))
  process.exit(0)
})

process.on("SIGINT", async function forceSell() {
  process.stdout.write("\n");
  if (modules.buyStatus == true && config.force_Sell == 1 && sellFired == false) {
    sellInitiated = true;
    log(chalk.red.bold("Force Selling"));
    await getBalanceAndSell(config.tokenToPurchase);
   // log(chalk.red.bold("Force Selling Tokens, Await"));
  } else {
    log(chalk.white.bold("Force Stop"));
  }
  process.exit();
});

