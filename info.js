const commandLineUsage = require("command-line-usage");
const sections = [
    {
        header: "Mirror Swap - Info",
        content: "Instructions & Guidelines:",
    },
    {
        header: "Usage: node trade -",
        content: `This Is A Simple Command Tool To Activate The Bot,
                  Each Function Is Activated With A Flag -,
                  And Each Function Will Respond At Your Command`
    },
    {
        header: "How To Set Up The Bot:",
        content: `You Will Need Two Different Tools, VS Code https://code.visualstudio.com/
                  And Node Js https://nodejs.org/en/`,
    },
    {
        header: "Setup And Getting Started:",
        content: `Setting Up The Sniper Bot:
                  
                  Download VS Code From The Link Above,
                  Download Node Js From The Link Above,

                  Open VS Code And Click On Open Folder, Then In The Main Directory Type npm i - Press Enter (Will Automatically Download The Modules)
                  
                  The Bot Have The Following Modules To Run In Different Instances:
                  
                  node trade -tx: Snipe On Any Projects That Did NOT Have A Presale Before, Liquidity Is Added In BNB Or ETH Or Matic
                  
                  node trade -btx: Snipe On Any Projects That Did NOT Have A Presale Before, Liquidity Is Added In BUSD
                  
                  node trade -dx: Snipe On Any Release From DxSale, You Will Need To Input The Developer Wallet Which You Can Find With The Following Below:
                  On DxSale App Go On Presale Address Of The Token You Want To Snipe, Click On It.. It Will Redirect You To BSC Scan, Go To Contract Creator
                  Than Click On TxHash, In The Next Page Copy The Address "from" And Copy It In The Bot .env File On dev_wallet, Than Run
                  node trade -dx (Before The Launch On Pancakeswap) And Let It Run Until Liquidity Is Found
                  
                  node trade -b: Send A Single Buy, If The Buy Fail You Can Set Up In Your .env File How Many Times You Want The Bot To Attempt Retrying The Buy
                  
                  node trade -op: Snipe On Any Projects That Open Trade Instead Of Adding Liquidity (Process Is Automatic)
                  
                  node trade -swap: A Module That Interact With A Smart Contract And Will Send 2 Single Swaps (Your purchaseAmount Divide 2 Times), To Setup The 
                  Contract, Have wbnb In Your Wallet, Approve wbnb Via Pancakeswap Router Than Proceed For Sniping`
    },
    {
        header: "Dependencies:",
        content: "npm i - This Will Install The Modules Necessary For The Bot To Function"

    },
    {
        header: "Variables:",
        content: `node trade -a     Approve Token On Request
                  node trade -tx    Activate txPool Sniping (Buying On Liquidity Pending Tx)
                  node trade -btx   Activate txpool Sniping where Liquidity Is Added In Busd
                  node trade -dx    Activate Sniping On DxSale Releases
                  node trade -b     Buy Only Modules (Purchase One Time)
                  node trade -op    Buy On Project That Use openTrading() 
                  node trade -ft    Buy Token With Personalised Contract Features
                `
    },
    {
        header: "Modules",
        content: `.env                  Configuration File
                  modules.js            Constructor
                  trade.js              Main File
                  detector              Rug Check File
                `
    },
    {
        header: "Bot Mode & Functions",
        content: `network =  Use 1 For Connecting To Bsc, 2 For Connecting To Matic, 3 For Connecting Eth
                  tokenToPurchase =  Input The Contract Address Of The Token You Wish To Buy
                  yourToken =  The Contract Address Of The Token We Are Using For Buying, NOTE: Main Currency Has To Be In Bnb, Matic Or Eth
                  purchaseAmount =  The Amount In (BNB, MATIC, ETH) We Wish To Buy
                  dx_Author =  The Dev Wallet Address Of DxSale Release We Want To Snipe To Find It: On DxSale App Search For:
                              Presale Address, Click On It Then, Contract Creator Click On The Hash, On The Next Window Copy The Address "from"
                              To The Bot Configuration
                  buyDelay =  0 To Deactivate, 1 To Activate A Delay Before Buying The (Token) Project
                  buyTimer =  To Combine With The Module Above "buyDelay" Input The Amount Of Second(s) You Wish To Delay Your Buy
                  buyRetries =  Number Of Retry Your Buy In Case Of Failure (Be Careful Of Possible Scam Token)
                  sellRetries = Number Of Retry Your Sell In Case Of Failure
                  retryMinTimeout =  Expressed in ms
                  retryMaxTimeout =  Expressed in ms
                  frontRunningMode =  0 To Deactivate, 1 To Add A Boost Of Gas To The Sniping
                  jumpBlocks =  0 To Deactivate, 1 Activate Skipping Blocks Modules To Fight High Taxes On First Blocks
                  for_x_blocks =  To Comine With Module "jumpBlocks" Input The Number Of Blocks To Skip
                  slippage =  Input The Slippage
                  gasPrice =  Input Gas Price To Use
                  gasLimit =  Input Gas Limit To Use
                  antiBot =  0 To Deactivate, 1 To Activate AntiBot Module
                  antiBot_Tx =  To Combine With Module "antiBot", Input How Many Time You Want To Split Your Transactions
                  sell_method =  1 To Instant Sell, 2 To Sell With Timer, 3 To Enable Holding Token And Monitoring Price (Trailing & Stop Loss)
                  sell_timer =  To Combine With sell_method 2, Input The Number In Second(s) You Want To Delay Your Sell
                  txPercentage =  Expressed In Percentage, A Module To Calculate Profit On Snipe, To Combine With sell_method 3
                  stop_loss =  Input The Percentage For Stop Loss
                  checkLiquidity = Input 1, Activate Liquidity Check (To Be Combined With Module Below)
                  liquidityScan =  0 To Deactivate, 1 To Activate A Module To Scan Liqudiity Removal (Protection From Rug Pull)
                  token_to_approve =  1 To Approve Before Buying
                  approve_before =  1 To Approve Before Selling
                  force_Sell =  Always Keep On 1, Enable Force Selling On ctrl+c
                  rugPull_scanner =  Always Keep On 1, Scan Contract Source For Possible Scam
                  functionTracker = Paste Here The Method ID Of The Token You Wish To Buy (Useful For Personalise Opening Trade Functions)
                  recipient =  Input Your Wallet Address
                  privateKey =  Input Your Metamask Wallet PrivateKey (No Seed Phrase!!)
                  node_ws =  Input The Websocket Address Of Your Node (Required For Sniping)
                  `
    },
    {
        header: "Version Number",
        content: "V.0.1.3",

    },
    {
        header: "Other Tools",
        optionList: [
            {
                name: "Mirror Swap",
                typeLabel: "{underline Mirror Trading}",
                description: "Tracking Whales And Follow That Specific Wallet For It To Execute x Amount Of BNB Transaction, Once The Bot Detect It It Will Front Run That Specific Buy, TxPool Integrated, And Spam Tx (In Development)",
            },
            {
                name: "Mirror Swap",
                typeLabel: "{underline Limit Trading Bot}",
                description: "A Trading Bot With Limit Orders, Trailing & Stop Loss (In Development)",
            },
        ]
    },
    {
        header: "Node Installation",
        optionList: [
            {
                name: "Binance Smart Chain",
                typeLabel: "{underline Full Node}",
                description: "Setting Up A Binance Smart Chain Full Node (Additional Cost 125 USD), System Requirements: RAM: 128 GB DDR4 ECC Hard drive: 2 x 1.92 TB NVMe SSD Datacenter Edition Connection: 1 GBit/s-Port Guaranteed bandwidth: 1 GBit/s https://www.hetzner.com/dedicated-rootserver/ax61-nvme",
            },
            {
                name: "Polygon",
                typeLabel: "{underline Full Node}",
                description: "Setting Up A Polygon Full Node (Additional Cost 175 USD), System Requirements: RAM: 128 GB DDR4 ECC Hard drive: 2 x 1.92 TB NVMe SSD Datacenter Edition Connection: 1 GBit/s-Port Guaranteed bandwidth: 1 GBit/s https://www.hetzner.com/dedicated-rootserver/ax61-nvme Or: 8 Physical Cores AMD EPYC 7282 2.8 GHz 64 GB RAM 480 GB NVMe SSD (With An Additional 1TB SSD Or Nvme) 1 Gbit/s Port 32 TB Traffic https://contabo.com/en/vds/vds-xl/?image=ubuntu.267&qty=1&contract=1", 
            },
            {
                name: "Kucoin KCC",
                typeLabel: "{underline Full Node}",
                description: "Setting Up A KCC Full Node (Additional Cost 115 USD), System Requirements: 256 GB NVMe 3 CPU 8192MB Memory 4000GB Bandwidth https://www.vultr.com/",
            },
            {
                name: "Ethereum",
                typeLabel: "{underline Full Node}",
                description: "Setting Up An Ethereum Full Node (Additional Cost 175 USD), System Requirements: **Refer To Smart Chain Requirements",
            },
        ]
    },
    {
        header: "General Comments",
        content: "Mirror Swap, It's Equipped With All The Latest Features Available, However The Tool Itself Does Not Guarantee Your Buy Will Be Succesfull Nor It Guarantee You Success In Every Snipe, Most Of The Dev Nowadays Use Very Advanced Contract, And New Feature Gets Added Every Day, However You Have The Most Of My Attention For Providing You The Most Up To Date Feature Which Gets Tested Regularly, Thank You For Purchasing And Happy Sniping"
    },

]
const info = (commandLineUsage(sections));
console.log(info);
